# Sample MT webapp

## To run
mvn org.eclipse.jetty:jetty-maven-plugin:run -Djetty.port=9000

## To use
Access 

- `http://localhost:9000/sample`
- `http://localhost:9000/session`
- `http://localhost:9000/cpu/54321`

Tenants are identified by hostname. Replace `localhost` with `127.0.0.1`
to demonstrate that the tenants have separate caches.
