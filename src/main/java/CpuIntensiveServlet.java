import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "cpu-intensive", urlPatterns = { "/cpu/*" })
public class CpuIntensiveServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String p = req.getPathInfo();

        if (p == null)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Try: /cpu/<number>");
            return;
        }

        p = p.replaceAll("\\D+", "");

        BigInteger product = BigInteger.valueOf(1);

        for (int i = 0; i < 1000; i++)
        {
            product = product.multiply(new BigInteger(p));
        }

        resp.setContentType("text/plain");
        resp.getWriter().write("Result is: " + product + "\n");
        resp.getWriter().write("Which is obviously: isProbablePrime=" + product.isProbablePrime(20) + "\n");
    }
}
