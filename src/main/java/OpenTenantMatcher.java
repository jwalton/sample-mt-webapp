import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.multitenant.TenantMatcher;
import com.atlassian.tenancy.api.Tenant;


public class OpenTenantMatcher implements TenantMatcher<Tenant>
{
    @Override
    public Tenant getTenantForRequest(HttpServletRequest request, HttpServletResponse response)
    {
        return new NamedTenantId(request.getServerName());
    }

    private static class NamedTenantId implements Tenant
    {
        private final String serverName;

        public NamedTenantId(String serverName)
        {
            this.serverName = serverName.toLowerCase(Locale.ENGLISH);
        }

        @Override
        public String name()
        {
            return serverName;
        }
    }
}
