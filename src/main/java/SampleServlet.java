import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.multitenant.NamedTenantContext;
import com.atlassian.multitenant.cache.SharedMultiTenantCacheManager;
import com.atlassian.tenancy.api.TenantContext;


@WebServlet(name = "sample", urlPatterns = { "/sample/*" })
public class SampleServlet extends HttpServlet
{
    TenantContext ref = NamedTenantContext.tenantContext();

    CacheManager cacheManager = new SharedMultiTenantCacheManager(ref, new MemoryCacheManager(), Collections.<String>emptySet());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");

        PrintWriter w = resp.getWriter();
        w.write("I totally am a servlet (" + req.getRequestURL() + ")\n");

        if (req.getParameter("flush") != null)
        {
            cacheManager.flushCaches();
            w.write("(and I flushed the caches)\n");
        }

//        w.write("Is set: " + ref.isSet() + "\n");
        w.write("Get: " + ref.getCurrentTenant() + "\n");
        w.write("Get: " + ref.getCurrentTenant().name() + "\n");
        w.flush();

        Cache cache = cacheManager.getCache("SampleServlet");

        Object current = cache.get("key");
        w.write("From the cache: " + current + "\n");
        if (current == null)
        {
            w.write("Calculating...\n");
            w.flush();

            StringBuffer sb = new StringBuffer();
            sb.append("From " + new Date().toString());
            try
            {
                Thread.sleep((long) (1500 + Math.random() * 1000));
            }
            catch (InterruptedException e)
            {
                throw new ServletException(e);
            }
            sb.append("... to " + new Date() + " (for " + ref.getCurrentTenant().name() + ")");

            cache.put("key", sb.toString());

            w.write("Cache now populated with: " + cache.get("key") + "\n");
        }
    }
}
