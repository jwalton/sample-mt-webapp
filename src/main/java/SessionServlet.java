import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "session", urlPatterns = { "/session" })
public class SessionServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        HttpSession sess = req.getSession();

        resp.setContentType("text/plain");

        Integer count = (Integer) sess.getAttribute("count");

        resp.getWriter().write("Currently stored: " + count + "\n");

        if (count == null)
        {
            resp.getWriter().write("Initialising\n");
            sess.setAttribute("count", Integer.valueOf(0));
        }
        else
        {
            resp.getWriter().write("Incrementing.\n");
            sess.setAttribute("count", Integer.valueOf(count.intValue() + 1));
        }
    }

}
