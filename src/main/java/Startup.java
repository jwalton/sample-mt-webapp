import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.atlassian.multitenant.impl.MTZero;


@WebListener
public class Startup implements ServletContextListener
{
//    static TenantIdStore<TenantId> store = new TenantIdStore<TenantId>();

    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        MTZero.init();
//        Properties props = new Properties();
//        props.setProperty(MultiTenantContext.SINGLE_TENANT_KEY, "false");
//        props.setProperty(MultiTenantContext.MULTI_TENANT_SYSTEM_HOME_PROPERTY, ".");
//        MultiTenantContext.defaultInit(props);
//
//        MultiTenantContext.init();
//        System.err.println("Initialised...");

//        NonReentrantNamedTenantReference tr = new NonReentrantNamedTenantReference();
//        TenantMatcher<NamedTenant> matcher = new OpenNamedTenantMatcher();
//
//        NamedTenantContext.init(tr, matcher, tr);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
    }
}
